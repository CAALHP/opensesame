﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using OpenSesame.WP8.Resources;
using Xamarin.Forms;

namespace OpenSesame.WP8
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

			Xamarin.Forms.Forms.Init();
			Content = OpenSesame.Core.App.GetMainPage().ConvertPageToUIElement(this);
        }

    }
}