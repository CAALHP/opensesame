using System;
using System.Threading.Tasks;

namespace OpenSesame.Core
{
	public interface IDoorService
	{
		Task OpenDoor (string roomNumber, string key);
	}
}