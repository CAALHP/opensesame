using System;

namespace OpenSesame.Core
{
	public interface IDoor
	{
		void OpenDoor (string key);
	}

}

