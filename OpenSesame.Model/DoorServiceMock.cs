using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using OpenSesame.Core;

namespace OpenSesame.Model
{
	public class DoorServiceMock : IDoorService
	{
		public async Task OpenDoor (string roomNumber, string key)
		{
            //const string baseUrl = "http://192.168.1.148:60773/door/open";
            //const string baseUrl = "http://192.168.1.136:60775/door/open";
		    const string baseUrl = "http://192.168.1.222:60775/door/open";

		    using (var httpClient = new HttpClient ()) 
			{
				try 
				{
					var valueCollection = new List<KeyValuePair<string,string>>
					{
					    new KeyValuePair<string, string>("room", roomNumber),
					    new KeyValuePair<string, string>("key", key)
					};
				    var content = new FormUrlEncodedContent(valueCollection);
					var postResponse = httpClient.PostAsync(baseUrl, content);
					var response = await postResponse;
				} 
				catch (System.Exception ex) {
                    //Maybe we should handle connection issues better...
					return;		
				}
			}
		}
	}
}