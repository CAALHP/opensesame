﻿using OpenSesame.Core;

namespace OpenSesame.Model
{
	public class Door : IDoor
	{
		private string _roomNumber;

		private IDoorService _doorService;

		public Door (string roomNumber, IDoorService doorService)
		{
			_doorService = doorService;
			_roomNumber = roomNumber;
		}

		public void OpenDoor(string key)
		{
			if (key.Length == 4)
				//call web service to open door
				_doorService.OpenDoor (_roomNumber, key);
		}
	}
}

