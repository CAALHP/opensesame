﻿using System;

namespace OpenSesame.Core
{
    public class Room
    {
        public Room(string address)
        {
            Address = address;
        }

        public String Address { get; private set; }
        public Pincode Pin { get; set; }
    }
}