﻿namespace OpenSesame.Core
{
    public class Pincode
    {
        public Pincode(int size)
        {
            Pin = size > 0 ? new char[size] : new char[4];
        }

        public char[] Pin { get; set; }
    }
}