﻿namespace OpenSesame.Core
{
    public class Logo
    {
        public Logo(string imagePath)
        {
            ImagePath = imagePath;
        }

        public string ImagePath { get; private set; }
    }
}