﻿using System.ServiceModel.Channels;
using Xamarin.Forms;

namespace OpenSesame.Core
{
	public partial class MainPage : ContentPage
	{
		public MainPage ()
		{
			InitializeComponent ();
		    BindingContext = App.Locator.Main;
		}
	}
}