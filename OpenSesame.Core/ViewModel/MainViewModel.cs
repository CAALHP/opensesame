﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using OpenSesame.Model;

namespace OpenSesame.Core.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly string _roomText;

        public string RoomText
        {
            get { return _roomText; }
        }

        private readonly string _roomNumber;

        public string RoomNumber
        {
            get { return _roomNumber; }
        }

        private readonly string _addressText;

        public string AddressText
        {
            get { return _addressText; }
        }

        private readonly string _addressLine1;

        public string AddressLine1
        {
            get { return _addressLine1; }
        }

        private readonly string _addressLine2;

        public string AddressLine2
        {
            get { return _addressLine2; }
        }

        private string _pin;

        public string Pin
        {
            get { return _pin; }
            set { Set(() => Pin, ref _pin, value); }
        }

        private RelayCommand _openDoorCommand;

        public RelayCommand OpenDoorCommand
        {
            get
            {
                return _openDoorCommand ?? (_openDoorCommand = new RelayCommand(OpenDoor));
            }
        }

        void OpenDoor()
        {
            _door.OpenDoor(Pin);
        }

        private readonly IDoor _door;

        public MainViewModel()
        {
            _roomText = "Room";
            _roomNumber = "339E";
            _addressText = "Address:";
            _addressLine1 = "Finlandsgade 22";
            _addressLine2 = "8200 Aarhus N";
            _door = new Door(_roomNumber, new DoorServiceMock());
        }

    }
}