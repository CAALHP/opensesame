﻿Feature: OpenDoor
	As a caregiver
	I want to open the door using OpenSesame
	In order to gain entry to a home
	
@mytag
Scenario: Add two numbers
	Given I have entered 50 into the calculator
	And I have entered 70 into the calculator
	When I press add
	Then the result should be 120 on the screen
